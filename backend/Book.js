const mongoose = require('mongoose')
const uuid = require('uuid/v4')
const Schema = mongoose.Schema;

const BookSchema = new Schema ({
    author: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    genre: {
        type: String,
        required: true
    },
    checkedOut: {
        type: Boolean,
        default: false
    },
    addedBy: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now()
    },
    _id: {
        type: String,
        default: uuid()
    },
})

module.exports = Book = mongoose.model('book', BookSchema)