const express = require('express');
const mongoose = require('mongoose');
const parser = require('body-parser')
const app = express()

//connect to db
const db = "mongodb+srv://christian_bahnweg:CrispyBacon!994@vis-library-wndt4.mongodb.net/test?retryWrites=true"
mongoose.connect(db, ).then(() => console.log('MongoDB connected!')).catch(err => console.log(err))

//bodyparser middleware
app.use(parser.json())

//import book model.
const Book = require('./Book')

app.get("/", (req, res) => {console.log("You've hit the homepage.")})
app.get("/catalog", (req, res) => {
    Book.find()
        .sort({title: -1})
        .then(books => res.json(books))
})
app.get("/catalog/:id", (req,res) => {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Book.findById(req.params.id)
        .then(book => {
            return res.json(book);
        })
        .catch(err => console.log(err)) 
      }
      else {
          console.log("Invalid Object Id")
      }
})

app.get("/add-book", (req, res) => res.send("You hit the book addition page"))
app.post("/catalog/", (req, res) => {
    const newBook = new Book({
        author: req.body.author,
        title: req.body.title,
        genre: req.body.genre,
        addedBy: req.body.addedBy
    })
    newBook.save().then(book => res.json(book)).catch(err => console.log(err))
})
app.delete("/:id", (req, res) => {
    Book.findOneAndDelete(req.params.id)
    .then(book => res.status(200).json(book))
    .catch(err => console.log(err))
})


app.get("/checkout", (req,res) => res.send("You've hit the checkout form."))
app.post("/checkout/:id", (req, res) => res.send("You've checked out a book."))

app.listen(5000, ()=> console.log("Listening on 5000."))